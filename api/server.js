const express = require('express')
const app = express()
const port = 3000
var cors = require('cors');
var fs = require('fs');

var data = JSON.parse(fs.readFileSync('./data.json', 'utf8'));

app.use(cors());

app.get('/products', (req, res) => res.send(data));

app.get('/products', (req, res) => res.send(req.query.name !== undefined ? data.Product.filter(product => product.name === req.query.name) : ""));

app.get('/products/:id', (req, res) => res.send(req.params.id !== undefined ? data.Product.filter(product => product.id == req.params.id)[0] : ""));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))