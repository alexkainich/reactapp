export const filterProduct = (product, activeFilters) => {
    if (activeFilters.length > 0) {
        const numericFilters = activeFilters.filter(filter => filter.type.trim() === "Numeric");
        const numericFiltersResults = (numericFilters.length > 0) ? numericFilters.map(filter => numericFilter(product, filter)) : [];
        const numericFiltersResult = (numericFiltersResults.length > 0) ? numericFiltersResults.filter(result => result === true).length === numericFilters.length : true;

        const textFilters = activeFilters.filter(filter => filter.type.trim() === "Text");
        const textFiltersResults = (textFilters.length > 0) ? textFilters.map(filter => textFilter(product, filter)) : [];
        const textFiltersResult = (textFiltersResults.length > 0) ? textFiltersResults.filter(result => result === true).length === textFilters.length : true;

        const optionsFilters = activeFilters.filter(filter => filter.type.trim() === "Options");
        const optionsFiltersResults = (optionsFilters.length > 0) ? optionsFilters.map(filter => optionsFilter(product, filter)) : [];
        const optionsFiltersResult = (optionsFiltersResults.length > 0) ? optionsFiltersResults.filter(result => result === true).length === optionsFilters.length : true;

        return numericFiltersResult && textFiltersResult && optionsFiltersResult;
    }

    return true;
}

const numericFilter = (product, filter) => {
    if (product[filter.name] && product[filter.name] > Number(filter.value))
        return true;
    else
        return false;
}

const textFilter = (product, filter) => {
    if (product[filter.name] && product[filter.name].includes(filter.value))
        return true;
    else
        return false;
}

const optionsFilter = (product, filter) => {
    if (product[filter.name] && product[filter.name].includes(filter.value))
        return true;
    else
        return false;
}