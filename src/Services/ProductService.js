const backendBaseUrl = 'http://localhost:1337';

class ProductService {
      getAllProducts(productCategory) {
            return fetch(`${backendBaseUrl}/products${productCategory ? "?categories.title=" + productCategory : ""}`)
                  .then(response => response.json())
                  .then(products => {
                        products = products.map(product => {
                              product['main-image'].url = `${backendBaseUrl}${product['main-image'].url}`;
                              return product;
                        })
                        return products;
                  })
      }

      getProductById(id) {
            return fetch(`${backendBaseUrl}/products/${id}`)
            .then(response => response.json())
            .then(product => {
                  product['main-image'].url = `${backendBaseUrl}${product['main-image'].url}`;
                  return product;
            })
      }

      getAllProductFilters() {
            return fetch(`${backendBaseUrl}/productfilters`)
            .then(response => response.json())
      }
}

export default new ProductService();