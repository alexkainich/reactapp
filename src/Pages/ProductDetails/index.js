import { connect } from 'react-redux';
import Component from './ProductDetails';
import { addToCart } from 'redux-actions';

const mapStateToProps = state => {
  return {
    cart: state.reducers.cart,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    callAddToCart(product) {
      dispatch(addToCart(product));
    },
  };
};

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);
export default Container;