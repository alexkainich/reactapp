import React, { Component } from 'react';
import Layout from 'page-layout';
import AddToCart from '../Components/AddToCart';
import ProductService from 'product-service';

class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {product: {title: 'Undifined', 'main-image': {url: ''}  }  };
  }

  componentWillMount() {
    const productId = this.props.match.params.id;
    ProductService.getProductById(productId).then(result => this.setState({product: result}));
  }

  render() {
    return (
      <Layout>
        <h1>{this.state.product.title}</h1>
        <p>{this.state.product.description}</p>
        <img src={this.state.product['main-image'].url} alt="Not found" />
        <AddToCart product={this.state.product}/>
      </Layout>
    );
  }
}

export default ProductDetails;
