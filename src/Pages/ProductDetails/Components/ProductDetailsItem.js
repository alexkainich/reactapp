import React from 'react';

class ProductDetailsItem extends React.Component {

  render() {
    const { name, value } = this.props;

    return (
        <li>{name}: {value}</li>
    );
  }
}

export default ProductDetailsItem;
