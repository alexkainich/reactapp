import React, { Component } from 'react';

class CartListItem extends Component {
    render() {
        return (
            <li>{this.props.product.title}: {this.props.product.price}
                <button onClick={() => {
                    this.props.callRemoveFromCart(this.props.product.id)
                }
                }> Remove from Cart</button>
            </li>
        );
    }
}

export default CartListItem;
