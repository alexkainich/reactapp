import { connect } from 'react-redux';
import Component from './cart';
import { removeFromCart, clearCart } from 'redux-actions';

const mapStateToProps = state => {
  return {
    cart: state.reducers.cart,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
      callRemoveFromCart(product) {
        dispatch(removeFromCart(product));
      },
      callClearCart(product) {
        dispatch(clearCart(product));
      },
    };
  };

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);
export default Container;