import React, { Component } from 'react';
import CartListItem from './Components/CartListItem';
import Layout from 'page-layout';

class Cart extends Component {
    renderCart() {
        return <div>
            <ul>{this.props.cart
                .map((product, index) => <CartListItem product={product} callRemoveFromCart={this.props.callRemoveFromCart} key={index} />)}
            </ul>
        </div>;
    }

    renderEmptyCart() {
        return "You have no products in your cart";
    }

    render() {
        const myCart = this.props.cart;
        var productList;

        if(myCart.length !== 0) {
            productList = this.renderCart();
        }
        else {
            productList = this.renderEmptyCart();
        }

        return (
            <Layout>
            <div className="ProductListWrapper">
                {productList}

                <br></br>                            
                <button onClick={() => {
                    this.props.callClearCart();
                }}>Clear cart</button>
            </div>
            </Layout>
        );
    }
}

export default Cart;
