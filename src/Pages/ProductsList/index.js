import { connect } from 'react-redux';
import Component from './ProductList';
import { updateProductList, updateCurrentProductCategory } from 'redux-actions';
import ProductService  from 'product-service';

const mapStateToProps = state => {
  return {
    activeProductFilters: state.reducers.productFilters.filter(filter => filter.value.length > 0),
    productsList: state.reducers.productsList,
    currentProductCategory: state.reducers.currentProductCategory,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchProductList(productCategory) {
      dispatch(updateCurrentProductCategory(productCategory));

      ProductService.getAllProducts(productCategory)
        .then(result => {
          dispatch(updateProductList(result));
        });
    },
  };
};

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);
export default Container;

