import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Col, Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button } from 'reactstrap';
import AddToCart from '../../Components/AddToCart';

class ProductListItem extends Component {

  render() {
    return (
      <Col xs={12} sm={12} md={6} lg={4} xl={3}>
        <Card className='ev-product-list-single-item'>
          <CardImg className='ev-product-list-image' top width="100%" src={this.props.product['main-image'].url} alt="Card image cap" />
          <CardBody>
            <Link to={`/products/${this.props.product.id}`}><CardTitle>{this.props.product.title}</CardTitle></Link>
            <CardSubtitle>£{this.props.product.price}</CardSubtitle>
            <CardText className='ev-product-description'>{this.props.product.description}</CardText>
            <Button><Link className='ev-view-product-button-text' to={`/product/${this.props.product.id}`}>View</Link></Button>
            <AddToCart product={this.props.product}/>
          </CardBody>
        </Card>
      </Col>
    );
  }
}

export default ProductListItem;