import React, { Component } from 'react';
import ProductListItem from './Components/ProductListItem';
import Layout from 'page-layout';
import { Badge, Row } from 'reactstrap';
import { filterProduct } from 'utils/ProductFiltering';

import './styles.scss';

class ProductList extends Component {

  render() {
    const productCategory = this.props.match.params.category || null;

    if (this.props.productsList.length === 0 || productCategory !== this.props.currentProductCategory) {
      this.props.fetchProductList(productCategory);
    }

    const productList = 
      <Row>
      { this.props.productsList
        .filter(product => filterProduct(product, this.props.activeProductFilters))
        .map(product => <ProductListItem id={product.id} product={product} key={product.id} />) }
      </Row>;

    return (
      <Layout sidebar={true}>
        <h1 className='ev-product-list-header'><Badge color="secondary">Our Products</Badge></h1>
        <div className='ev-product-list-wrapper'>
          { this.props.productsList.length > 0 ? productList : "Loading..." }
        </div>
      </Layout>
    );
  }
}

export default ProductList;
