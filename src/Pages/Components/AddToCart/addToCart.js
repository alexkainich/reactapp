import React, { Component } from 'react';

class AddToCart extends Component {

    render() {
        return(
            <button onClick={() => {
                this.props.callAddToCart(this.props.product);
            }}>
              Add to cart
            </button>
        )
    }
}

export default AddToCart;
