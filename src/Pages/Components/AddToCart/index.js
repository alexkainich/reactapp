import { connect } from 'react-redux';
import Component from './addToCart';
import { addToCart } from 'redux-actions';

const mapStateToProps = (state, ownProps) => {
  return {
      product: ownProps.product,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    callAddToCart(product) {
      dispatch(addToCart(product));
    },
  };
};

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);
export default Container;