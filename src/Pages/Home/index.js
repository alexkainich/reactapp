import { connect } from 'react-redux';
import Component from './Home';

const mapStateToProps = state => {
  return {
    productsList: state.reducers.productsList,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  };
};

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);
export default Container;