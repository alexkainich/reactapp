import React, { Component } from 'react';
import Layout from 'page-layout';

class ContactUs extends Component {
  render() {
    return (
      <Layout>
        <div> You can find us here: ... </div>
      </Layout>
    );
  }
}

export default ContactUs;
