import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';

import ProductList from './Pages/ProductsList';
import ProductDetails from './Pages/ProductDetails';
import Cart from './Pages/Cart';
import Home from './Pages/Home';
import ContactUs from './Pages/ContactUs';

import './styles.scss';
import reducers from "./redux"; // Gets the State from the reducer(s)
import { createStore } from "redux";
const store = createStore(reducers); // Creates the store from the State received from the reducer(s)

class App extends Component {
  render() {
    return (
      <Provider store={store} >
          <Router>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/products" component={ProductList} />
                <Route exact path="/products/:category" render={(props) => (<ProductList {...props} category={props.match.params.category} />  )}/> {/* so that the component will re-render */}
                <Route exact path="/product/:id" component={ProductDetails} />
                <Route exact path="/cart" component={Cart} />
                <Route exact path="/contactus" component={ContactUs} />
              </Switch>
          </Router>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
