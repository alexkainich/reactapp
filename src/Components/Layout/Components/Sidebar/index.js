import { connect } from 'react-redux';
import Component from './Sidebar';
import { addProductFilters, updateProductFilter } from 'redux-actions';
import ProductService  from 'product-service';

const mapStateToProps = state => {
  return {
      productFilters: state.reducers.productFilters,
    }
  };

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
      updateProductFilter(id, value) {
        dispatch(updateProductFilter(id, value));
      },

      fetchProductFilters() {
        ProductService.getAllProductFilters()
          .then(result => {
            dispatch(addProductFilters(result));
          });
      },
    };
  };

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);
export default Container;