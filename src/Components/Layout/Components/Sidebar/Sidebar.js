import React, { Component } from 'react';
import './styles.scss';
import FilterProductsList from './Components/FilterProductsList';

class Sidebar extends Component {

  componentDidMount() {
    if (this.props.productFilters.length === 0) {
      this.props.fetchProductFilters();
    }
  }

  render() {
    const productFilters = 
    <aside>
      { this.props.productFilters
        .map(filter => <FilterProductsList filter={filter} updateProductFilter={this.props.updateProductFilter} key={filter.id}/>) }
    </aside>

    return (
      <div>
        { this.props.productFilters.length > 0 ? productFilters : "Loading..." }
      </div>
    );
  }
}

export default Sidebar;