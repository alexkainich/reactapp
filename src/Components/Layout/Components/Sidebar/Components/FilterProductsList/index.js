import React, { Component } from 'react';
import './styles.scss';
import { Form, FormGroup, Input } from 'reactstrap';

class FilterProductsList extends Component {

    renderNumericFilter()
    {
        return <FormGroup className='ev-product-filter-input'><Input type="text" onChange={(e) => {this.props.updateProductFilter(this.props.filter.id, e.target.value)}} /></FormGroup>;
    }

    render() {
        return (
            <Form inline className='ev-product-filter'>
                {'Filter by: ' + this.props.filter.name}
                { this.renderNumericFilter() }
            </Form>
        )
    }
}

export default FilterProductsList;
