import React, { Component } from 'react';
import './styles.scss';
import { Badge, ListGroupItem, NavLink, Nav, NavItem, DropdownMenu, UncontrolledDropdown, Collapse, DropdownItem, Navbar, NavbarBrand, NavbarToggler, DropdownToggle } from 'reactstrap';
import { Link } from 'react-router-dom';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    return (
      <nav>
        <Navbar color="light" light expand="md">
          <Link to="/"><NavbarBrand>Evalest Eshop</NavbarBrand></Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Link to="/"><NavLink>Home</NavLink></Link>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Products
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <Link to="/products"><NavLink>All</NavLink></Link>
                  </DropdownItem>
                  <DropdownItem>
                    <Link to="/products/New"><NavLink>New</NavLink></Link>
                  </DropdownItem>
                  <DropdownItem>
                    <Link to="/products/Featured"><NavLink>Featured</NavLink></Link>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <NavItem>
                <Link to="/contactus"><NavLink>Contact Us</NavLink></Link>
              </NavItem>
              <NavItem>
                <Link to="/cart"><NavLink>
                <ListGroupItem className="justify-content-between">Cart <Badge pill>{this.props.cart.length}</Badge></ListGroupItem>
                </NavLink></Link>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </nav>
    );
  }
}

export default Header;
