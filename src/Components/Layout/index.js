import React, { Component } from 'react';
import Footer from './Components/Footer';
import Header from './Components/Header';
import Sidebar from './Components/Sidebar';
import { Col, Row } from 'reactstrap';
import './styles.scss';

class Layout extends Component {
    render() {
        const sizeWsb = (this.props.sidebar === true) ? 9 : 12;


        return (
            <div className='ev-layout'>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12} className='ev-header-wrapper'>
                        <Header />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={3} md={3} lg={3} xl={3} className={(this.props.sidebar === true) ? 'ev-sidebar-show' : 'ev-sidebar-hide'}>
                        <Sidebar />
                    </Col>    
                    <Col xs={12} sm={sizeWsb} md={sizeWsb} lg={sizeWsb} xl={sizeWsb} className='ev-main-content-wrapper'>
                        <section>{this.props.children}</section>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12} className='ev-footer-wrapper'>
                        <Footer />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Layout;
