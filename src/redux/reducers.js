import { CART_ADD, CART_REMOVE, CART_CLEAR, PRODUCTS_LIST_UPDATE, ADD_PRODUCT_FILTERS, UPDATE_PRODUCT_FILTER, UPDATE_CURRENT_PRODUCT_CATEGORY } from "./ActionTypes";
import { combineReducers } from "redux";

export const reducers = (state = {}, action) => {
  return combineReducers({
    cart: (state = [], action) => {
      const newCart = state.slice();
      switch (action.type) {
        case CART_ADD: 
          newCart.push(action.product);
          return newCart;
        case CART_REMOVE:
          let indexOfProductToRemove = state.findIndex((product) => product.id === action.id);
          newCart.splice(indexOfProductToRemove, 1);
          return newCart;
        case CART_CLEAR:
          return state = [];
        default:
          return state;
      }
    },
     productsList: (state = [], action) => {
       switch (action.type) {
         case PRODUCTS_LIST_UPDATE:
           return action.productsList;
         default:
           return state;
       }
     },
     productFilters: (state = [], action) => {
      const newFilters = state.slice();
      switch (action.type) {
        case ADD_PRODUCT_FILTERS:
          let productFiltersWithValueField = action.productFilters.map(filter => {
            filter.value = "";
            return filter;
          });
          return productFiltersWithValueField;
        case UPDATE_PRODUCT_FILTER:
          let indexOfFilterToUpdate = state.findIndex((filter) => filter.id === action.id);
          newFilters[indexOfFilterToUpdate].value = action.value;
          return newFilters;
        default:
          return state;
      }
    },
    currentProductCategory: (state = null, action) => {
      switch (action.type) {
        case UPDATE_CURRENT_PRODUCT_CATEGORY:
          return action.productCategory;
        default:
          return state;
      }
    },
  })(state, action);
};
