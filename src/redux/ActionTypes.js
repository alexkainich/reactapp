// ActionTypes are defined in constants.

// They are imported in Actions and Reducers.
// This prevents errors if they are misspelled over here.

/* eslint-disable */

export const CART_ADD = "CART_ADD_PRODUCT";
export const CART_REMOVE = "CART_REMOVE_PRODUCT";
export const CART_CLEAR = "CART_CLEAR_PRODUCTS";

export const PRODUCTS_LIST_UPDATE = "PRODUCTS_LIST_UPDATE";
export const PRODUCTS_LIST_FILTER = "PRODUCTS_LIST_FILTER";

export const ADD_PRODUCT_FILTERS = "ADD_PRODUCT_FILTERS";
export const UPDATE_PRODUCT_FILTER = "UPDATE_PRODUCT_FILTER";

export const UPDATE_CURRENT_PRODUCT_CATEGORY = "UPDATE_CURRENT_PRODUCT_CATEGORY";

/* eslint-enable */