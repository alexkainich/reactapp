// Actions are payloads of information that send data from your application to your store. 
// They are the only source of information for the store.

// Actions are triggered either by user through interactions or by an events, such as successful AJAX call.

// Read more on Actions - https://redux.js.org/docs/basics/Actions.html

import { CART_ADD, CART_REMOVE, CART_CLEAR, PRODUCTS_LIST_UPDATE, ADD_PRODUCT_FILTERS, UPDATE_PRODUCT_FILTER, UPDATE_CURRENT_PRODUCT_CATEGORY } from "./ActionTypes";

export function addToCart(product) {
  return {
    type: CART_ADD,
    product: product
  };
}

export function removeFromCart(id) {
  return {
    type: CART_REMOVE,
    id: id
  };
}

export function clearCart() {
  return {
    type: CART_CLEAR,
  };
}

export function updateProductList(productsList) {
  return {
    type: PRODUCTS_LIST_UPDATE,
    productsList: productsList
  };
}

export function addProductFilters(productFilters) {
  return {
    type: ADD_PRODUCT_FILTERS,
    productFilters: productFilters
  };
}

export function updateProductFilter(id, value) {
  return {
    type: UPDATE_PRODUCT_FILTER,
    id: id,
    value: value
  };
}

export function updateCurrentProductCategory(productCategory) {
  return {
    type: UPDATE_CURRENT_PRODUCT_CATEGORY,
    productCategory: productCategory
  };
}
 
